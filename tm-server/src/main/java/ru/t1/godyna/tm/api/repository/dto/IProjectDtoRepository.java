package ru.t1.godyna.tm.api.repository.dto;

import ru.t1.godyna.tm.dto.model.ProjectDTO;

public interface IProjectDtoRepository extends IUserOwnedDtoRepository<ProjectDTO> {

}
